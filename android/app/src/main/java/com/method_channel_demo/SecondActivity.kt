package com.method_channel_demo

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class SecondActivity : AppCompatActivity() {
    lateinit var editText: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val textView = findViewById<TextView>(R.id.flutterText)

        val data = intent?.getStringExtra("data")
        textView.text = "Recieved from flutter: $data"

        editText = findViewById<EditText>(R.id.nativeText)

        findViewById<Button>(R.id.sendToFlutterBtn).setOnClickListener {
            if (editText.text.isNotEmpty()) {
                setResult(RESULT_OK, intent?.putExtra("data", editText.text.toString()))
                finish()
            }
       }
    }

    override fun onBackPressed() {
        if (editText.text.isNotEmpty()) super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
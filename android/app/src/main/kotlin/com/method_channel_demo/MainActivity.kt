package com.method_channel_demo

import android.R.attr
import android.content.Intent
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity1: FlutterActivity() {
    private val CHANNEL = "flutter.native/helper"

    lateinit var channelResult: MethodChannel.Result;
    
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            channelResult = result
            if (call.method.isNotEmpty()) {
                val greetings = call.method
                startNewActivity(greetings)
            }
        }
    }

    private fun startNewActivity(result: String) {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("data", result)
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("MainActivity1.onActivityResult ---> $requestCode --resultCode-- ${resultCode == RESULT_OK}")
        if (requestCode == 1 && resultCode == RESULT_OK) {

            val strEditText: String = data?.getStringExtra("data") ?: ""
            println("MainActivity1.onActivityResult ---> $strEditText")

            channelResult.success(strEditText)
        }
    }
}
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'native_response_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
// This is the theme of your application.
//
// Try running your application with "flutter run". You'll see the
// application has a blue toolbar. Then, without quitting the app, try
// changing the primarySwatch below to Colors.green and then invoke
// "hot reload" (press "r" in the console where you ran "flutter run",
// or simply save your changes to "hot reload" in a Flutter IDE).
// Notice that the counter didn't reset back to zero; the application
// is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

// This widget is the home page of your application. It is stateful, meaning
// that it has a State object (defined below) that contains fields that affect
// how it looks.

// This class is the configuration for the state. It holds the values (in this
// case the title) provided by the parent (in this case the App widget) and
// used by the build method of the State. Fields in a Widget subclass are
// always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('flutter.native/helper');
  int _counter = 0;
  TextEditingController textEditingController = TextEditingController();
  String _responseFromNativeCode = 'Waiting for Response...';
  StreamController<String> streamController = StreamController.broadcast();

  void _incrementCounter() {
    setState(() {
// This call to setState tells the Flutter framework that something has
// changed in this State, which causes it to rerun the build method below
// so that the display can reflect the updated values. If we changed
// _counter without calling setState(), then the build method would not be
// called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  void initState() {
// TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
// This method is rerun every time setState is called, for instance as done
// by the _incrementCounter method above.
//
// The Flutter framework has been optimized to make rerunning build methods
// fast, so that you can just rebuild anything that needs updating rather
// than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
// Here we take the value from the MyHomePage object that was created by
// the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            TextField(
              controller: textEditingController,
              decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)),
                  labelText: 'Enter Text',
                  suffixStyle: const TextStyle(color: Colors.green)),
            ),
            SizedBox(height: 20,),
            RaisedButton(
                child: Text('Send to Native'),
                onPressed: () async {
                  String response = "";
                  try {
                    final String result = await platform.invokeMethod(textEditingController.text ?? '');
                    response = result;
                  } on PlatformException catch (e) {
                    response = "Failed to Invoke: '${e.message}'.";
                  }
                  streamController.add(response);
                }),
            Container(
              margin: EdgeInsets.only(top: 50),
              child: NativeResponseWidget(nativeResponse: streamController.stream,),
            )
          ]), // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }

  @override
  void dispose() {
    super.dispose();
    streamController.close();
  }
}
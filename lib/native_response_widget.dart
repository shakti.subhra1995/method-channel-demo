import 'package:flutter/material.dart';

class NativeResponseWidget extends StatelessWidget {
  final Stream<String> nativeResponse;

  const NativeResponseWidget({Key key, this.nativeResponse}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: nativeResponse,
        builder: (context, snapshot) {
          return Text('Native Result -> ${snapshot.data ?? ''}');
        });
  }
}